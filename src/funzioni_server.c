/**
 * @file     funzioni_server.c
 * @brief    Libreria di un server 
 * 
 * @author   Matteo Maso (<matteo.maso.1@studenti.unipd.it>)
 * @version  1.0
 * @since    1.0
 *
 * @details  Libreria per un server che deve ricere comandi ed elaborarli
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdbool.h>

/*librerie per opencv */
#include <cv.h>
#include <highgui.h>
#include <cxcore.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/videoio/videoio_c.h>
#include <opencv2/highgui/highgui_c.h>

/* declares the funcions provided by this library */
#include "funzioni_server.h"

#define DEB     1 //se DEB = 1 allora sono abilitati i print del debug
#define SCRITTE 1 //se lo abilito abilita le scritte lato server, in futuro non servirebbe se lo eseguo di nascosto



//per l udp


#define MAXSTRINGLENGTH 70  //da stabilire la lunghezza



/**
 * @brief return a message on stderr
 *
 * @param msg    the address of the first value. The value will not be modified.
 * @param detail the address of the second value. The value will not be modified.
 */
void DieWithUserMessage(const char *msg, const char *detail) {
    fputs(msg, stderr);
    fputs(": ", stderr);
    fputs(detail, stderr);
    fputc('\n', stderr);
    exit(1);
}

/**
 * @brief return a message 
 *
 * @param msg    the address of the value. The value will not be modified.
 */
void DieWithSystemMessage(const char *msg) {
    perror(msg);
    exit(1);
}


/**
 * @brief INVIO STRINGA TRAMITE TCP
 *
 * @param command  array che termina per '\0'               
 * @param sock     puntatore ad una sock                    gli passo direttamente il valore della socket da usare
 * @details        invio la stringa terminante con '\0'
 */  
void send_TCP(char *command, int sock){
             
    size_t  strLen   = strlen(command);                    //determina la lunghezza della stringa da mandare      
    ssize_t numBytes = send(sock, command, strLen, 0);     //invia la stringa al server 
    //if(DEB)printf("\nLunghezza della stringa inviata in bytes: %d", numBytes);
    if(numBytes < 0 )
        DieWithSystemMessage("send() failed");
    else if (numBytes != strLen)
        DieWithUserMessage("send()", "sent unexpected number of bytes");
} 

/** 
 * @brief RICEZIONE STRINGA TRAMITE TCP
 *
 * @param length     se ho int x = 0; gli passo &x
 * @param sock       puntatore ad una sock se ho sock = socket() gli passo sock
 * @param buffer     in teoria se alloco un buffer con void *buf = malloc(x); gli passo buf se ho char buffer[] passo &buffer
 * @param bufLength  bufLength è la lunghezza max del baffer in cui scrivo 
 * @details          ricevo su sock la stringa di lunghezza length e lunghezza massima buflength
 */ 
void recv_TCP(char *buffer, int sock, int *length, int bufLength ){

    //ssize_t recv(int socket, void *buffer, size_t length, int flags);               
    //dovrei verificare cosa succede se mando più cose di bufLength
    //in caso le faccio scartare e dico stringa troppo lunga

    ssize_t numBytes = recv(sock, buffer, (size_t)bufLength, 0 );   //inviata la stringa
    *length = numBytes;                                             //inserito il numero di bytes ricevuti all indirizzo di length

    if(numBytes < 0 )
        DieWithSystemMessage("send() failed");
    //devo aggiornare l ultimo carattere solo se diverso da zero se il numBytes -1 = '\0' ok altrimenti lo metto io 
    if(DEB)printf("\nnumBytesrecv = %i\n", (int)numBytes);            
    if(!((buffer[numBytes - 1]) == '\0') && numBytes > 0){ //se l ultimo carattere nn è '\0' ma ho ricevuto bytes
        if(numBytes < bufLength){   //sistemo l ultimo indice se non raggiungo la dimensione max         
            buffer[numBytes] = '\0';        //devo verificare se punta bene a quel che deve puntare 
        }else{
            DieWithUserMessage(" 0 buffer", "dimensione massima buffer raggiunta");    
        }
    }

} 

/**
 * @brief   CREA UNA SOCKET server TCP 
 *
 * @param   socketName      punta ad un descrittore socket  dovrò passarlo con l indirizzo
 * @poram   port            passo argv[1] esempio  oppure se ho char porta[] = "4445" gli passo porta
 * @details                 crea una socket TCP in ascolto sulla porta che gli passo e la pone in listen
 * @return                  0 if ok -1 if errore
 */
int createMySocketTCP(int *servSock , char port[]){

    int MAXPENDING = 1;                                              //numero massimo di connessioni nel buffer

    int servSock3;                                                   // Socket descriptor for server
    servSock3 = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);           //costruisco la socket
    if (servSock3 < 0) 
        DieWithSystemMessage("socket() failed");                     //errore nella creazione della socket
    
    in_port_t servPort = atoi(port);
    if(SCRITTE)printf("\nPorta usata: %d\n", servPort);

    // apre la porta lato server per accettare connessione fuori da una lan 
    char var[30];
    snprintf(var, 30, "ufw allow %i ", servPort);       //creo la stringa del comando
    system(var);                                        //mando comando a sistema di apertura porta
    
    // Create socket for incoming connections  del server 
    printf("\nFinito socket \n");    
    //printf("\nservSock1 vale = %i\n", servSock3);    
    // Construct local address structure
    struct sockaddr_in servAddr;                            //Local address
    memset(&servAddr, 0, sizeof(servAddr));                 //Zero out structure
    servAddr.sin_family = AF_INET;                          //IPv4 address family
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);           //Any incoming interface
    servAddr.sin_port = htons(servPort);                    //Local port    

    int option = 1;                                                              //Impostazioni
    setsockopt(servSock3, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));    //per riutilizzare la porta
                                                                                                    
    if(bind(servSock3, (struct sockaddr*) &servAddr,  sizeof(servAddr)) < 0 )    //Bind sulla servSock
         DieWithSystemMessage("bind() failed");

                                                     //la listen non è bloccante
    if (listen(servSock3, MAXPENDING) < 0)           //Mark the socket so it will listen for incoming connections
        DieWithSystemMessage("listen() failed");     //continua a mettere nel buffer le connessioni che poi tolgo con l accept
    *servSock = servSock3;

    return 0;
} 

/**
 * @brief FUNZIONE DI SINCRONIZZAZIONE CLIENT
 * 
 * @param sockClient    socket del client in cui mandare le risposte
 * @param structDati    gli passo l'indirizzo del nome della mia struttura dati es struct matteo ciao passo &ciao
 */
void connessioneClient( int sockClient, struct DATI *dati_pointer){

    if(DEB)printf("\nfunzione connessioneClient in corso... \n");
    int BUFSIZE = 2000;                                     // #Max bytes of buffer stringhe ricevute
    char *buffer = (char *)malloc(sizeof(char) * BUFSIZE);  //alloco la memoria per il buffer caratteri ricevuti

    char lista_comandi[] = "comandi implementati lato server: \
\n -help_server                mi restituisce la lista di comandi implementati lato server\
\n -client_UDP_port porta      deve salvarsi la porta che gli mando\
\n -sinc                       avvia sincronizzazione\
\n -start                      termina programma e va nel pipe\
\n -server_stop                cessa la sincronizzazione senza aver finito\
\n ";

    char *separate = " ";       //dichiaro i separatori per lo string tokenaizer
    int setup = 0;
  
    while(!setup){
        //qui devo ricevere dei comandi e alla ricezione eseguire delle azioni
        //esco dal while solo quando ricevo un determinato programma
        printf("\n dentro al ciclo while di connessione client\n");
        setup = 1;      //significa setup eseguito oppure uscita 
        int numBytesRcvd; // = recv(sockClient, buffer, BUFSIZE, 0); //ricevi un messaggio dal cliente
        recv_TCP( buffer , sockClient , &numBytesRcvd, BUFSIZE );        
        if(DEB)printf("\nbytes ricevuti = %i\n", (int)numBytesRcvd);
        if(numBytesRcvd < 0)
            DieWithSystemMessage("recv() failed");
        
        

        printf("\nStringa ricevuta = %s\n", buffer);
        char *arg1 = strtok(buffer, separate);
        char *arg2 = strtok(NULL,   separate);
        //if(DEB)printf("\narg1 = -%1$s-\narg2 = -%2$s-\n", arg1, arg2); 
        //devo aggiornare l ultimo carattere solo se diverso da zero se il numBytes -1 = '\0' ok altrimenti lo metto io 
                
        
        //if(SCRITTE) printf("\nStringa ricevuta = %s\n", buffer);
        
        if(numBytesRcvd > 0){                                                     //eseguo funzioni in base al primo comando rcvd 
            if(!strcmp( arg1, "server_stop")){ //*********************************** SERVER_STOP *****************************
                if(SCRITTE)printf("\nRicevuta la stringa server_stop\n");           //lascio a zero control cosi esco dal ciclo
            }else{
                setup = 0; //imposto il control = 0 cosi proseguo ed elaboro le richieste
                
                if(!strcmp( arg1, "help_server")){ //******************************* HELP_SERVER *****************************
                    if(SCRITTE){ printf("\nStringa ricevuta = %s\n", buffer); }
                    send_TCP( lista_comandi , sockClient);
                     
                }else if(!strcmp( arg1, "client_UDP_port")){ //******************* CLIENT_udp_port poi dovrò eliminarla**********
                    if(SCRITTE){ printf("\nPorta ricevuta = %s\n", arg2); }           
                    //aggiungere controlli sulla stringa ricevuta per vedere se è una porta valida              
                    in_port_t por = atoi(arg2);                 //formatto la porta ricevuta      
                    (*dati_pointer).clientPortUDP_video = por;  //assegno la porta UDP del client alla struttura del server
                    send_TCP( "porta acquisita con successo" , sockClient);
                    if(DEB)printf("\nPorta udp client: -%s-\n", arg2);
                }else if(!strcmp( arg1, "avvio_udp")){ // ************************ AVVIO_UDP da eliminare***************
                    if(SCRITTE){ printf("\nStringa ricevuta = %s\n", buffer); }
                //***********************devo associare porta ed indirizzo del mio client alla struttura per udp
         
                    //(*dati_pointer).avvio_udp = 1; //imposto ad 1 la variabile della struct che mi indica di avviare l udp 
                    send_TCP( "fine_sinc_avvenuta_con_successo" , sockClient);  //fa capire al client che è andato tutto ok
                    setup = 1;                     //ed esco dal ciclo 
                }else if(!strcmp(arg1 , "porta_udp_server")){       //per ora nn implementata dal client
                    if(SCRITTE) printf("\nporta_udp_server\n");    
                    in_port_t servPortUDP_video = (*dati_pointer).servPortUDP_video ;   
                    char stringa[10];  //contengo la stringa da mandare al server 
                    snprintf(stringa, 10 , "%i", servPortUDP_video );                 
                    send_TCP( stringa  , sockClient);                   //invia al client una stringa contentente la porta udp
                }else if(!strcmp(arg1 , "sinc")){       //inizia la sincronizzazine dal client
                    if(DEB) printf("\nsinc ricevuta\n");                 
                    send_TCP( "ok_sinc"  , sockClient);              
                }else if(!strcmp(arg1 , "get_Size_Header")){       //mando la dimensione del header
                    if(DEB) printf("\nget_Size_Header\n");
                    char sizeHeader[20];
                    snprintf(sizeHeader, 10, "%i", (*dati_pointer).nSize);
                    if(DEB)printf("\nsizeheader mandato -%s-\n", sizeHeader);
                    if(DEB)printf("\nsizeheader reale -%i-\n", (*dati_pointer).nSize);
                    sleep(5);                 
                    send_TCP( sizeHeader , sockClient);     //devo mandare la dimensione del frame
                    if(DEB) printf("\ndimensione header mandata: %s\n", sizeHeader);         
                }else if(!strcmp(arg1 , "client_UDP_video")){      //ricevo la porta udp video client la salvo e glie la rimando
                    (*dati_pointer).clientPortUDP_video = atoi(arg2); //memorizzo la porta UDP del client video
                    if(DEB) printf("\nRicevuta porta video udp client: -%i-\n", (int)((*dati_pointer).clientPortUDP_video));
                    send_TCP( arg2  , sockClient);  //rispedisco la porta che ho acquisito
                                 
                }else if(!strcmp(arg1 , "get_Header")){      //devo mandare l header    
                    ssize_t numBytest = send( sockClient , (*dati_pointer).frame , (*dati_pointer).nSize , 0);//invia la stringa al server 
                    if(numBytest < 0 )
                        DieWithSystemMessage("send() failed");
                    else if (numBytest != (*dati_pointer).nSize)
                        DieWithUserMessage("send()", "sent unexpected number of bytes");

                    if(DEB)printf("\nInviato header\n");
                }else if(!strcmp(arg1 , "sincronizzazione_finita")){      //devo rispondere sincronizzazione finita
                    //in caso rispondo dopo eventuali controlli, per es se ho la porta udp video del client
                    send_TCP( "ok_fine_sincronizzazione"  , sockClient);                    
                    if(DEB)printf("\nInvio stringa di fine sincronizzazione\n");    
                }else if(!strcmp(arg1 , "start")){       //da finire di implementare, esce dala fnzione
                    if(SCRITTE) printf("\nporta_udp_server\n");                
                    //send_TCP( stringa  , sockClient);                   //invia al client una stringa contentente la porta udp
                    setup = 1; //esco dalla funzione eventulmente controllo qualcosa
                }else{                                                                  //stringa nn valida do nothings
                    if(SCRITTE) printf("\nStringa ricevuta = %s non valida\n", buffer);
                    send_TCP( "stringa_non_valida" , sockClient);
                }
            }       //fine else analisi stringhe ricevute
        }           //fine analisi ciclo con numbytes > 0
    }               //funzione di setup completata
    free(buffer);   //libero la memoria allocata per il buffer di ricezione;

    //devo anche settare un parametro per avviare o meno i due processi finito questa funzione se esco senza averli settati devo settarli di default
}

//************************************ FINE SISTEMAZIONE FUNZIONI ****************************************************



/**
 * @brief   CREA UNA SOCKET server UPD
 *
 * @param   socketName      punta ad un descrittore socket
 * @poram   port            passo argv[1] esempio  oppure se ho char porta[] = "4445" gli passo porta
 *
 * @return                  0 if ok -1 if errore
 */
void createMySocketUDP(int *servSockUDP , char port[]){

    if(DEB)printf("\nFunzione createMySocketUDP in corso...\n");
    in_port_t servPort = atoi(port);
    if(DEB)printf("\nport passata a createMysocket: -%s-\n", port);
    if(DEB)printf("\nport passata a createMysocket: -%i-\n", servPort);
    // Construct the server address structure
    struct sockaddr_in  servstruct;                   //Criteria for address
    memset(&servSockUDP, 0, sizeof(servstruct));       //Zero out structure
    servstruct.sin_family       = AF_INET;
    servstruct.sin_port         = htons(servPort);                 
    servstruct.sin_addr.s_addr  = htonl(INADDR_ANY);               

    // Create socket for incoming connections
    int sock = socket(PF_INET , SOCK_DGRAM ,IPPROTO_UDP );
    if (sock < 0)
        DieWithSystemMessage("socket() failed");

    // Bind to the local address
    if (bind(sock, (struct sockaddr *)&servstruct, sizeof(servstruct)) < 0)
        DieWithSystemMessage("bind() failed");  

    if(DEB)printf("\nport passata a createMysocket: -%i-\n", servPort);

    int option = 1;                                                             //Impostazioni
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));        //per riutilizzare la porta
    if(SCRITTE)printf("\nFinito settaggio impostazioni socket\n");
    *servSockUDP = sock; //assegno la socket creata alla socket che mi hanno passato //qui da problemi 
       
    if(SCRITTE)printf("\nFinita la funzione create my socketUDP\n");
}



/**
 * @brief esaudisce delle funzioni passate alla socket
 *
 * @param clntSocket   The value identify the clntSocket
 * @param mypipefd     punta alla pipe
 * @param structDati    gli passo il nome del puntatore alla strututra dati
 */
void HandleTCPClient( int clntSocket, int mypipefd[], struct DATI *dati_pointer){
    
    if(DEB)printf("\nfunzione HandleTCPClient in corso... \n");
    int BUFSIZE = 2000;                                     // #Max bytes of buffer stringhe ricevute
    char *buffer = (char *)malloc(sizeof(char) * BUFSIZE);  //alloco la memoria per il buffer caratteri ricevuti
    
    char lista_comandi[] = "comandi implementati lato server: \
\n -help_server                mi restituisce la lista di comandi implementati lato server\
\n -server_stop                spegne la trasmissione dei frame su udp ed esce dal ciclo e dal programma\
\n ";

    char *separate = " ";
    int setup = 0;
    while(!setup){
        sleep(1);         
        if(DEB)printf("\nwhile funzione HandleTCPClient in corso... \n");
        //qui devo ricevere dei comandi e alla ricezione eseguire delle azioni
        //esco dal while solo quando ricevo un determinato programma

        setup = 1;      //significa setup eseguito oppure uscita 
        int numBytesRcvd; // = recv(sockClient, buffer, BUFSIZE, 0); //ricevi un messaggio dal cliente
        recv_TCP( buffer , clntSocket , &numBytesRcvd, BUFSIZE );        
        if(DEB)printf("\nbytes ricevuti = %i\n", (int)numBytesRcvd);
        if(numBytesRcvd < 0)
            DieWithSystemMessage("recv() failed");
        
        if(DEB)printf("\nComando ricevuto = -%s-\n", buffer);
        char *arg1 = strtok(buffer, separate);
        char *arg2 = strtok(NULL,   separate);

        if(numBytesRcvd > 0){        
            if(!strcmp( arg1 , "server_stop")){
                if(SCRITTE)printf("\nRicevuta la stringa server_stop\n"); //lascio a zero control cosi esco dal ciclo
                //mando comando di spegnimento all'udp e lo lascio uscire da tutto 
                close(mypipefd[0]);
                write(mypipefd[1], "stop_udp", strlen("stop_udp"));
                //(*dati_pointer).avvio_udp = 0;      //fa morire il ciclo dell'udp o muore cosi o leggendo da pipe di morire
                send_TCP( "Spegnimento server in corso..." , clntSocket);
            }else{
                setup = 0; //imposto il control = 0 cosi proseguo ed elaboro le richieste
                
                if(!strcmp( arg1, "help_server")){
                    if(SCRITTE){ printf("\nStringa ricevuta = %s\n", buffer); }
                    send_TCP( lista_comandi , clntSocket);                   
                }else {
                    send_TCP( "comando_ricevuto_non_valido" , clntSocket);   //torna una stringa di comando non valido
                }
            } //fine else
        } //fine if opzioni 
    } //fine while ciclo continuo 
    free(buffer); //libero la memoria usata dal buffer stringhe 
} //fine funzione


void server_echo_udp(int sockfd, struct sockaddr *p_cliaddr, socklen_t clilen){
    
    int MAXLINE = 50;    

    int n;
    socklen_t len;
    char mesg[MAXLINE];
    for (;;) {
        len = clilen;
        if ( (n = recvfrom(sockfd, mesg, MAXLINE, 0, p_cliaddr, &len) ) < 0){
            printf("recvfrom error \n"); exit(1);
        }
        if( sendto(sockfd, mesg, n, 0, p_cliaddr, len) != n ){
            printf("sendto error\n"); exit(1);
        }
    }
}
