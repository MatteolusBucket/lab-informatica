/**
 * @file client_01.c
 * @brief client di connessione a server di videosorveglianza
 *
 * @author Matteo Maso (<matteo.maso.1@studenti.unipd.it>)
 * @version 2.0
 * @since 2.0
 *
 */

/* libc include for the printf function */
#include <stdio.h> 

/* libc include for the memory functions */
#include <stdlib.h>

/* libc include for the string functions */
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdbool.h>

/* libc include for video function */
#include <cv.h>
#include <highgui.h>
#include <cxcore.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/videoio/videoio_c.h>
#include <opencv2/highgui/highgui_c.h>

/* declares the funzioni_client library  for client function */
#include "funzioni_client.h" 

/* definisco variabili globali */ //capire bene cosa sono
#define MAX_PIPE_LEN 50         //lunghezza massima buffer pipe

                                               
//devo mettere le variabili globali ma nn so se si chiamao così

/**
 * @brief Si collega a un server di video sorveglianza
 *
 * @return the exit code from the program, \c 0 if no errors occurred.
 *
 * @author Matteo Maso (<matteo.maso.1@studenti.unipd.it>)
 * @version 2.0
 * @since 2.0
 */
int main(int argc, char *argv[]){

    if (argc < 3 || argc > 5)       //controllo parametri avvio
        DieWithUserMessage("Parameter(s)", "<Server Address> <Server PortTCP> <Debug option 1/0>");
    //Third arg optionale default debug disable
    int DEB = (argc == 4) ? atoi(argv[3]) : 1; //per ora metto di default 1 ma poi mettero 0

    //stampo i parametri inseriti 
    printf("\nParametri di lavoro inseriti: \n\tIndirizzo server %1$s Port %2$s\n\tDebug option %3$s\n", argv[1] , argv[2], argv[3]);

    /* create tcp connection */
    int socketTCP; //identificatore socket TCP 
    if( (createTCP( &socketTCP, argv[2], argv[1])) < 0 ){     
        DieWithUserMessage("Function createTCP", "errore nella creazione socket TCP");
    } else {
        if(DEB)printf("\nCreazione socket TCP avvenuta con successo\n");
    }


    /*Struttura dati */
    struct DATI_CLNT dati_clnt; //struttura dati client
    //struct sockaddr_in servAddr;                                //struttura sockaddr_in dati server TCP 
    //inizializzo struttura dati  
    dati_clnt.stato_udp   = 0;                                    //stato udp socket accesa o spenta
    dati_clnt.stato_video = 0;                                    //stato video spento 
    //dati_clnt.sockUDPserver_pointer = &servAddr;                //verificare ma punta alla mia struttura struct server
    //dati_clnt.len_sockUDP = (socklen_t)sizeof(servAddr);        //ha la lunghezza della struttura dati 
    dati_clnt.sock_TCP = socketTCP;                               //socketTCP identifier
    
    /* riservo memoria per il buffer */
    IplImage *frame = (IplImage *)malloc(200);    //forse posso allocarlo quando mi serve oppure reallocare la memoria se è troppa me vedo
    dati_clnt.frame = frame;                      //metto l indirizzo del frame nella struttura dati

    if( connect_server_TCP(socketTCP, &dati_clnt) < 0){                     //funzione di sincronizzazione TCP
        if(DEB)printf("\nERRORE NELLA SINCRONIZZAZIONE -> chiusura in corso \n");
        //fare qualcosa per non entrare nel resto del programma ma chiudere le cose allocate
    }else{
        if(DEB)printf("\nconnect_server avvenuta con successo\n");
    }
    
    /* Creazione pipe */
    pid_t pid;
    int fd1[2];                                                     //padre  -> [___] -> figlio
    int fd2[2];                                                     //figlio -> [___] -> padre
    int ret;
    char *pipe_buf = (char *)malloc(sizeof(char) * MAX_PIPE_LEN);   //alloco memoria buffer pipe
    ret = pipe(fd1);                                                //controllo errori creazione fd1
    if(ret == -1){ DieWithSystemMessage("pipe() failed"); }
    ret = pipe(fd2);                                                //controllo errori creazione fd2
    if(ret == -1){ DieWithSystemMessage("pipe() failed"); } 
    fcntl(fd1[0], F_SETFL, O_NONBLOCK);        //set opzione pipe non bloccante in lettura fd1 
    fcntl(fd2[0], F_SETFL, O_NONBLOCK);        //set opzione pipe non bloccante in lettura fd2  
    if(DEB)printf("\nCreazione Pipe eseguita");
    pid = fork();                                                   //Creazione processi padre e figlio  
    if(DEB)printf("\nFork eseguito");

    if(pid == 0){
                                                        /* PROCESSO FIGLIO */ 
        if(DEB)printf("\nProcesso figlio creato");        
        if(dati_clnt.sinc_status == 0){ // se mi son sincronizzato ok inizio udp e faccio tutto il resto altrimenti no

            /*Inizializzo l immagine  */
            int c;
            cvNamedWindow("window", CV_WINDOW_AUTOSIZE);            
            cvNamedWindow("Window", CV_WINDOW_AUTOSIZE); //vedo se posso toglierne una in caso 
            char *frame_buffer = (char *)malloc((*frame).imageSize); //alloco un buffer per contenere ciò ceh ricevo da UDP
            //l header dovrebbe essere già posizionato sul frame 


            /* CREO UDP */
            struct addrinfo addrCriteria;                       // Criteria for address match
            memset(&addrCriteria, 0, sizeof(addrCriteria));     // Zero out structure
            addrCriteria.ai_family = AF_UNSPEC;                 // Any address family
            addrCriteria.ai_socktype = SOCK_DGRAM;              // Only datagram sockets
            addrCriteria.ai_protocol = IPPROTO_UDP;             // Only UDP protocol
           
            struct addrinfo *servAddrs; // List of server addresses
            int rtnVal = getaddrinfo(argv[1], dati_clnt.portaUDPserver, &addrCriteria, &servAddrs);
            if (rtnVal != 0)
                DieWithUserMessage("getaddrinfo() failed", gai_strerror(rtnVal));  
            int sock = socket(servAddrs->ai_family, servAddrs->ai_socktype,servAddrs->ai_protocol); // Socket descriptor for client
            if (sock < 0)
                DieWithSystemMessage("socket() failed");
            if(DEB)printf("\nFinita creazione socket udp\n");
    
            //devo dire al padre che ho finito di fare la socket udp così lui lo dice al server
            {
                write_pipe(fd2, "udp_pronta", sizeof("udp_pronta")); //dico al padre che ho finito l udp 
            }
    
            int stop = 1;
            while(stop){
                //controllo la pipe e fin che il padre non mi dice che l'udp del server è pronta sto fermo 
            }
            

            struct sockaddr_storage fromAddr; // Source address of server
            socklen_t fromAddrLen = sizeof(fromAddr);
            size_t  remlen;
            size_t sendlen;
            void *curpos;
            int len;
            stop = 1;
            while(stop){
                read_pipe_unblock(fd1, pipe_buf);   //legge se il padre gli dice qualcosa
                if(!strcmp(pipe_buf, "stop_figlio")){
                   if(DEB)printf("\nRicevuto stop_udp sul figlio e ora lo spengo..\n");
                       stop = 0; //esco dal ciclo 
                }else{
                  
                    sendto(sock, "get_frame", sizeof("get_frame") , 0, servAddrs->ai_addr, servAddrs->ai_addrlen); 
                    sendlen = (*frame).imageSize;       //inizializza il contatore del buffer
                    remlen  = (*frame).imageSize;       //inizializza il punto el buffer in cui sono arrivato 
                    curpos = frame_buffer;

                    while (remlen > 0)
                    {
                        len = recvfrom(sock, curpos, 1024, 0 , (struct sockaddr *) &fromAddr, &fromAddrLen);
                        curpos += len;
                        remlen -= len;
                        sendlen = MIN(remlen, 1024);
                    }
                                
                    (*frame).imageData = frame_buffer; 
                    cvShowImage("Window",frame);
                    cvWaitKey(1);
                }

            } //fine while

            cvDestroyAllWindows();
            free(frame_buffer); //libero memoria allocata per il buffer
            close(sock);    //libero la socket UDP creata
        } 
        exit(0); //chiusura processo figlio 
    } else {
                                                        /* PROCESSO PADRE */
        if(DEB)printf("\nProcesso padre creato"); 
        
        //da completare 

        //devo continuare ad ascoltare il server il figlio e i comandi da tastiera
        //appena il figlio ha creato l udp lo dico al server che parte a sua volta
        //appena il server ha ultimato l udp lo dico al figlio che parte ed inizia lo streaming udp

        wait(); //aspetta che il figlio si chiuda prima di terminare
        if(DEB)printf("\nProcesso padre terminato");
    } //chiusura processo padre
    
    if(DEB)printf("\nTerminazione del programma in corso...\n");
    free(frame);
    free(pipe_buf);   //libero memoria allocata per il buffer pipe
    close(socketTCP); //chiusura socketTCP 
    return 0;
}

