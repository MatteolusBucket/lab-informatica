/**
 * @file     funzioni_server.h
 * @brief    Libreria di un server 
 * 
 * @author   Matteo Maso (<matteo.maso.1@studenti.unipd.it>)
 * @version  1.0
 * @since    1.0
 *
 * @details  Libreria per un server che deve ricere comandi ed elaborarli
 */


/******** INIZIALIZZO UNA STRUTTURA DI TIPO DATI   devo spostarla da qui poi */
struct DATI {
        //dati server 
        in_port_t servPortTCP;          //porta server TCP
        in_port_t servPortUDP_video;    //porta server UDP_video
        
        //dati immagine server
        IplImage *frame;                //puntatore alla struttura IplImage
        int nSize;                      //contiene la dimensione del header in bytes da mandare al client        
        int imageSize;
        int nChannel;
        int width;
        int height;
        
        //dati client
        char                 *ipclient;                   //indirizzo del client
        in_port_t            clientPortUDP_video;         //porta client UDP_video
        struct sockaddr_in   *sockUDPclient_pointer;      //punta alla struttura dati udp del client********da verificare ******
        socklen_t            len_sockUDP;                 //contiene i dati della lunghezza della struttura *****da verificare
        int                  avvio_udp;
           
};


/**
 * @brief return a message on stderr
 *
 * @param msg    the address of the first value. The value will not be modified.
 * @param detail the address of the second value. The value will not be modified.
 */
void DieWithUserMessage(const char *msg, const char *detail);

/**
 * @brief return a message 
 *
 * @param msg    the address of the value. The value will not be modified.
 */
void DieWithSystemMessage(const char *msg);

/**
 * @brief INVIO STRINGA TRAMITE TCP
 *
 * @param command  array che termina per '\0'               
 * @param sock     puntatore ad una sock                    gli passo direttamente il valore della socket da usare
 * @details        invio la stringa terminante con '\0'
 */  
void send_TCP(char *command, int sock); 

/** 
 * @brief RICEZIONE STRINGA TRAMITE TCP
 *
 * @param length     se ho int x = 0; gli passo &x
 * @param sock       puntatore ad una sock se ho sock = socket() gli passo sock
 * @param buffer     in teoria se alloco un buffer con void *buf = malloc(x); gli passo buf se ho char buffer[] passo &buffer
 * @param bufLength  bufLength è la lunghezza max del baffer in cui scrivo 
 * @details          ricevo su sock la stringa di lunghezza length e lunghezza massima buflength
 */ 
void recv_TCP(char *buffer, int sock, int *length, int bufLength );

/**
 * @brief   CREA UNA SOCKET server TCP 
 *
 * @param   socketName      punta ad un descrittore socket  dovrò passarlo con l indirizzo
 * @poram   port            passo argv[1] esempio  oppure se ho char porta[] = "4445" gli passo porta
 * @details                 crea una socket TCP in ascolto sulla porta che gli passo e la pone in listen
 * @return                  0 if ok -1 if errore
 */
int createMySocketTCP(int *servSock , char port[]);

//************************************ FINE SISTEMAZIONE FUNZIONI ****************************************************

/**
 * @brief esaudisce delle funzioni passate alla socket
 *
 * @param clntSocket   The value identify the clntSocket
 * @param mypipefd     punta alla pipe
 * @param structDati    gli passo il nome del puntatore alla strututra dati
 */
void HandleTCPClient(int clntSocket, int mypipefd[], struct DATI *dati_pointer);

/**
 * @brief apre una socket udp e resta in ascolto fin che un nn si connette, poi inizia a mandargli frame
 *
 * @param service      punta all'array di una porta da utilizzare
 * @param mypipefd[]   punta al primo elelemento della pipe
 */
//void udp_socket(char *service, int mypipefd[]);

void print_webcam_info(CvCapture* cv_cap);

/**
 * @brief   CREA UNA SOCKET server UPD
 *
 * @param   socketName      punta ad un descrittore socket
 * @poram   port            passo argv[1] esempio  oppure se ho char porta[] = "4445" gli passo porta
 *
 * @return                  0 if ok -1 if errore
 */
void createMySocketUDP(int *servSockUDP , char port[]);


/**
 * @brief FUNZIONE DI SINCRONIZZAZIONE CLIENT
 * 
 * @param sockClient    socket del client in cui mandare le risposte
 * @param structDati    gli passo il nome del puntatore alla strututra dati
 */
void connessioneClient(int sockClient,  struct DATI *dati_pointer);

void server_echo_udp(int sockfd, struct sockaddr *p_cliaddr, socklen_t clilen);
