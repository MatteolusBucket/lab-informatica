/**
 * @file     server.c
 * @brief    Programma server
 * 
 * @author   Matteo Maso (<matteo.maso.1@studenti.unipd.it>)
 * @version  1.0
 * @since    1.0
 *
 * @details  Programma di server che manda streaming video via udp ad un client
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdbool.h>
/*librerie per opencv */
#include <cv.h>
#include <highgui.h>
#include <cxcore.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/videoio/videoio_c.h>
#include <opencv2/highgui/highgui_c.h>


#include "funzioni_server.h" //libreria Matteo Maso server

#define MAXSTRINGLENGTH 70  //da stabilire la lunghezza
#define DEB             1   //se DEB = 1 allora sono abilitati i print del debug
#define SCRITTE         1   //se lo abilito abilita le scritte lato server, in futuro non servirebbe se lo eseguo di nascosto

int BUFSIZE = 2000;            //numero max di bytes del buffer di stringhe inviate

int main(int argc, char *argv[]){

    if (argc != 3) // Test for correct number of arguments
        DieWithUserMessage("Parameter(s)", "<Server PortTCP> <Server PortUDP>");

    int servSock;                                       //socket TCP
    int x = createMySocketTCP( &servSock , argv[1] );   //creo la socket TCP
    //controllo errori su x da aggiungere
          
    struct sockaddr_in clntAddr;                        //creo la struttura per accettare il client in connessione
    socklen_t clntAddrLen = sizeof(clntAddr);           //set length of client address structure (in-out of parameter) 

    //wait for a client to connect se il buffer è vuoto aspetta altrimenti procede con la prima richiesta che trova la 
    int clntSock = accept(servSock, (struct sockaddr *) &clntAddr, &clntAddrLen); // accettazione mette i dati del client 
    if(clntSock < 0)                                                              // nella struttuca sockaddr 
        DieWithSystemMessage("accept() failed");                                  // oppure manda un mex d errore se falisce
        
    /* Ora il server è connesso con un client */
    char clntName[INET_ADDRSTRLEN]; // String to contain client address    
    if(SCRITTE){    //se è abilitato mostra dei parametri al terminale lato server altrimenti no
        if (inet_ntop(AF_INET, &clntAddr.sin_addr.s_addr, clntName, sizeof(clntName)) != NULL)
            printf("Handling client %s/%d\n", clntName, ntohs(clntAddr.sin_port));   //stampo dati client connesso e porta
        else    
            puts("Unable to get client address"); }         //se non ho agganciato il client manda errore          
     
//DA RIVEDERE
    //********** DEFINISCO LA STRUTTURA DATI DEPUTATA A CONTENERE INFORMAZIONI CHE POSSO DARE AL CLIENT E CHE POSSO IMMAGAZZINARE
    struct DATI dati;   
    dati.servPortUDP_video = atoi(argv[2]);             //assegno la porta udp che uso alla mia struttura dati


    //************** INIZIALIZZARE LA FOTOCAMERA  *******************************************************************
    CvCapture* capture = cvCaptureFromCAM(0);       //inizializzo la webcam
    IplImage* frame;                                //inizializzo un frame di tipo iplImage
    frame = cvCloneImage(cvQueryFrame(capture));    //assumo un frame dalla webcam cosi posso associare i dati alla mia struttura
    dati.nSize = (*frame).nSize;                    //imposto la dimensione del header    
    if(DEB)printf("\nsu inizializza fotocamera ho: (*frame).nSize -%1$i-  e dati.nSize -%2$i-\n", (*frame).nSize, dati.nSize);   
    dati.frame = frame;                             //imposto il puntatore al mio frame nella struttura 
    

    if(DEB)printf("\n porta memorizzata nella struttura dati: %u\n", dati.servPortUDP_video);
    
    connessioneClient( clntSock , &dati );
   //  if(DEB)printf("\n porta memorizzata nella struttura dati per  udp del client: %i\n", (int)dati.clientPortUDP_video);
    cvReleaseCapture(&capture); //oppure potrei proprio salvare tutto un header nella struttura ... ma nn mi va
    

//uscito da questa funzione creo pipe, sul figlio faccio girare l udp che continua a mandare frame sul padre tcp continua ad ascoltare comandi tcp 

    // CREO DUE PROCESSI PADRE TCP E FIGLIO SU UDP tramite le pipe
    pid_t pid;              //parametri pipe
    int mypipefd[2];        //vettore identificatore pipe
    int ret;                //parametri pipe
    char buf[20];           //parametri pipe
    ret = pipe(mypipefd);   //inizializza la pipe
    if(ret == -1){          //controlla che l inizializzazione sia andata a buon fine 
        perror("pipe");
        exit(1);
    }   
    fcntl(mypipefd[0], F_SETFL, O_NONBLOCK); //serve a rendere la pipe non bloccante in lettura
    pid = fork();  //avvia il processo figlio 
    if(DEB)printf("\nCreazione pipe finita\n");
    
    if(pid == 0){
        //************************** PROCESSO FIGLIO **************************************************
        printf("\nChild Process\n");
            
        //inizializzo la fotocamera
        CvCapture* capture = cvCaptureFromCAM(0);       //inizializzo la webcam
        IplImage* frame1;                                //inizializzo un frame di tipo iplImage
        frame1 = cvCloneImage(cvQueryFrame(capture));    //assumo un frame dalla webcam cosi posso associare i dati alla mia 
        sleep(5);        
        

        int nSize         = (*frame1).nSize;    
        int nChannels     = (*frame1).nChannels;
        int depth         = (*frame1).depth;
        int width         = (*frame1).width;
        int height        = (*frame1).height;
        int imageSize     = (*frame1).imageSize;
        char* imageData   = (*frame1).imageData;
        int widthStep     = (*frame1).widthStep;
    
        printf("\nnSize:      %i\n", nSize);
        printf("\nnChannels:  %i\n", nChannels);
        printf("\ndepth:      %i\n", depth);
        printf("\nwidth:      %i\n", width);        
        printf("\nheight:     %i\n", height);
        printf("\nimageSize:  %i\n", imageSize);
        printf("\nwidthStep:  %i\n", widthStep);

        //INIZIO CREAZIONE UDP 
        char *service = argv[2]; // First arg: local port/service
        // Construct the server address structure
        struct addrinfo addrCriteria;                       //Criteria for address
        memset(&addrCriteria, 0, sizeof(addrCriteria));     //Zero out structure
        addrCriteria.ai_family   = AF_UNSPEC;               //Any address family
        addrCriteria.ai_flags    = AI_PASSIVE;              //Accept on any address/port
        addrCriteria.ai_socktype = SOCK_DGRAM;              //Only datagram socket
        addrCriteria.ai_protocol = IPPROTO_UDP;             //Only UDP socket

        struct addrinfo *servAddr;                          // List of server addresses
        int rtnVal = getaddrinfo(NULL, service, &addrCriteria, &servAddr);
        if (rtnVal != 0)
            DieWithSystemMessage("getaddrinfo() failed");
        // Create socket for incoming connections
        int sock = socket(servAddr->ai_family, servAddr->ai_socktype, servAddr->ai_protocol);
        if (sock < 0)
            DieWithSystemMessage("socket() failed");
        // Bind to the local address
        if (bind(sock, servAddr->ai_addr, servAddr->ai_addrlen) < 0)
            DieWithSystemMessage("bind() failed");
        // Free address list allocated by getaddrinfo()
        freeaddrinfo(servAddr);

        //QUI ANDREI DENTRO AL FOR
        struct sockaddr_storage clntAddrs; // Client address
        // Set Length of client address structure (in-out parameter)
        socklen_t clntAddrLeng = sizeof(clntAddrs);
        // Block until receive message from a client
        char buffer[MAXSTRINGLENGTH]; // I/O buffer SARÀ IL BUFFER PER LA RICEZIONE
        buffer[0] = '\0'; //inizializzo il buffer;

        // apre la porta lato server per accettare connessione fuori da una lan 
        char var[30];
        snprintf(var, 30, "ufw allow %s ", argv[2]);       //creo la stringa del comando
        system(var);                                        //mando comando a sistema di apertura porta
    

        //FINE CREAZIONE UDP 
        //qui dovrei mandare un messaggio al padre e dire che sono pronto e lui lo manda al figlio 

        if(DEB)printf("\nEntro nel while e invio cose\n");

        size_t sendlen = 1024;
        size_t remlen  = 921600;
        const void *curpos; 
        ssize_t len;
        ssize_t numBytesRcvd;
        int s = 1;
        while(s){
            
            close(mypipefd[1]);
            read(mypipefd[0], buf, 15);
              
            if(!strcmp(buf, "stop_udp")){
                printf("\ndentro al while figlio tcp stop\n");
                s = 0;
            }else{
                
                
                numBytesRcvd = recvfrom(sock, buffer, MAXSTRINGLENGTH, 0, (struct sockaddr *) &clntAddrs, &clntAddrLeng);
                
                frame1 = cvCloneImage(cvQueryFrame(capture));                
    
                curpos = (*frame1).imageData;
                remlen  = 921600;
                while (remlen > 0)
                {
                    len = sendto(sock, curpos, sendlen, 0 , (struct sockaddr *) &clntAddrs, sizeof(clntAddrs));
                    curpos += len;
                    remlen -= len;
                    sendlen = MIN(remlen, 1024);
                }     
            }
        } //fine while s

        // CHIUDO TUTTE LE COSE APERTE DENTRO AL FIGLIO sinc        
        cvReleaseCapture(&capture);
        
        
       
        if(DEB)printf("\nProcesso figlio finito\n");
        exit(0);
    } else {
    
            //****************************** PROCESSO PADRE ***********************************
           // if(DEB)printf("\nParent process avviato\n");   
            HandleTCPClient( clntSock, mypipefd, &dati);  //funzione che cicla e accetta richeste
            wait(); //in teoria aspetta la terminazione del figlio prima di chiudersi 
            if(DEB)printf("\nParent process end\n");
            exit(0);
    } //graffa dell else
    //uscito dai due processi padre e anche figlio
    if(DEB)printf("\nUscito dai processi padre e figlio\n");


    if(DEB)printf("\nChiusura del programma in corso...\n");
   
    cvReleaseCapture(&capture);     //chiudo la fotocamera
    close(clntSock);                //chiudo la socket clientTCP
    close(servSock);                //chiudo la socket serverTCP
    return 0;
}  



