/**
 * @file     client.c
 * @brief    client tcp che manda comandi  e  deve ricevere streaming video su socket udp
 * 
 * @author   Matteo Maso (<matteo.maso.1@studenti.unipd.it>)
 * @version  1.0
 * @since    1.0
 *
 * @details  Programma di client che si collega ad un server via tcp, acquisisce dati a video con udp e manda comandi al server
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdbool.h>

/*librerie per opencv */
#include <cv.h>
#include <highgui.h>
#include <cxcore.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/videoio/videoio_c.h>
#include <opencv2/highgui/highgui_c.h>

#include "funzioni_client.h" //libreria client di Matteo Maso
//#define MAXSTRINGLENGTH 70  
#define DEB 1   //se DEB = 1 allora sono abilitati i print del debug

/* da sistemare queste son per la udp */
#define BUFLEN 1000000
#define MSGS 5	/* number of messages to send */
#define SERVICE_PORT	9001	/* hard-coded port number */

int BUFSIZE = 50;


int main(int argc, char *argv[]){
    
    if (argc < 4 || argc > 5) // Test for correct number of arguments
        DieWithUserMessage("Parameter(s)", "<Server Address> <Server PortTCP> <Client PortUDP"); 
    

    int sockTCP;                                                //socket TCP con cui posso scrivere al server
    int df = createMySocketTCP( &sockTCP , argv[2], argv[1]);   //creo socket TCP alla porta argv[1]

    struct sockaddr_in servAddr;                                //struttura sockaddr_in UDP server
    socklen_t servAddrLen = sizeof(servAddr);                   //set length of server address structure 

    struct DATI_CLNT dati_clnt;                                 //struttura dati del client
    //struct DATI_CLNT *dati_clnt_pointer = &dati_clnt;         //puntatore alla struttura dati che passerò alle funzioni 
    dati_clnt.stato_video = 0;                                  //stato video spento 
    dati_clnt.sockUDPserver_pointer = &servAddr;                //verificare ma punta alla mia struttura struct server
    dati_clnt.len_sockUDP = servAddrLen;                        //ha la lunghezza della struttura dati 
    dati_clnt.portaUDPclnt = atoi(argv[3]);                     //dovrebbe contenere la porta udp del client
    dati_clnt.sock_TCP = sockTCP;                               //socketTCP identifier
    dati_clnt.sinc_status = 0;                                  //stato della sincronizzazione
    
    //inizializzo lo spazio per un  frame 
    IplImage *frame = (IplImage *)malloc(160);                  //da vedere la dimensione       
    //frame = cvCreateImageHeader(cvSize(640, 480), 8, 3);        //forse non serve inizializzarlo
    dati_clnt.frame = frame;                                    //metto nella struttura l indirizzo del mio puntatore al frame
    
    connessioneServer(sockTCP, &dati_clnt);                     //funzione di sincronizzazione TCP
    if(DEB)printf("\nFinita connessione\n");

        int nSize         = (*frame).nSize;    
        int nChannels     = (*frame).nChannels;
        int depth         = (*frame).depth;
        int width         = (*frame).width;
        int height        = (*frame).height;
        int imageSize     = (*frame).imageSize;
        char* imageData   = (*frame).imageData;
        int widthStep     = (*frame).widthStep;
    
        printf("\n1nSize:      %i\n", nSize);
        printf("\n1nChannels:  %i\n", nChannels);
        printf("\n1depth:      %i\n", depth);
        printf("\n1width:      %i\n", width);        
        printf("\n1height:     %i\n", height);
        printf("\n1imageSize:  %i\n", imageSize);
        printf("\n1widthStep:  %i\n", widthStep);


    

   
    // inizio dei processi acquisizione stringhe parte tcp ed avvio udp figlio 
    pid_t pid;
    int mypipefd[2];
    int ret;
    char buf[20];
    ret = pipe(mypipefd);
    if(ret == -1){      //controllo se ci son stati errori sotto
        perror("pipe");
        exit(1);
    } 
    fcntl(mypipefd[0], F_SETFL, O_NONBLOCK); //serve a rendere la pipe non bloccante in lettura    
    pid = fork();  //crea il processo figlio 
    
    
    if(pid == 0){
        
            /******************************************* child process ****************************************************/
            if(DEB)printf("\nChild Process\n");
            //inizializzo fotocamera devo mettere una sincronizzazione 
            int c;
            cvNamedWindow("window", CV_WINDOW_AUTOSIZE);
            //IplImage *frame1;            
            cvNamedWindow("Window", CV_WINDOW_AUTOSIZE);  
            //frame = cvCreateImageHeader(cvSize(640, 480), 8, 3);
            memcpy(frame, dati_clnt.frame , 144);    //mi prendo l header ricevuto e messo nella struttura dati          
            //cvCreateData(frame1);
            //frame = cvInitImageHeader(frame, cvSize(640, 480), 8, 3, 0, 4); 
            char *frame_buffer = (char *)malloc(921600);
            /*
            int nSize         = (*frame1).nSize;    
            int nChannels     = (*frame1).nChannels;
            int depth         = (*frame1).depth;
            int width         = (*frame1).width;
            int height        = (*frame1).height;
            int imageSize     = (*frame1).imageSize;
            char* imageData   = (*frame1).imageData;
            int widthStep     = (*frame1).widthStep;
    
            printf("\nnSize:      %i\n", nSize);
            printf("\nnChannels:  %i\n", nChannels);
            printf("\ndepth:      %i\n", depth);
            printf("\nwidth:      %i\n", width);        
            printf("\nheight:     %i\n", height);
            printf("\nimageSize:  %i\n", imageSize);
            printf("\nwidthStep:  %i\n", widthStep); */
       
            
            //INIZIO CREAZIONE SOCKET UDP
            int MAXSTRINGLENGTH = 10;
            char *server = argv[1];    // First arg: server address/name
            char *echoString = "ciao"; // Second arg: word to echo
            size_t echoStringLen = strlen(echoString);
            if (echoStringLen > MAXSTRINGLENGTH) // Check input length
                DieWithUserMessage(echoString, "string too long");
            // Third arg (optional): server port/service
            char *servPort = argv[3]; //porta udp del server che poi acquisirà tramite tcp
            // Tell the system what kind(s) of address info
            struct addrinfo addrCriteria;                       // Criteria for address match
            memset(&addrCriteria, 0, sizeof(addrCriteria));     // Zero out structure
            addrCriteria.ai_family = AF_UNSPEC;                 // Any address family
            addrCriteria.ai_socktype = SOCK_DGRAM;              // Only datagram sockets
            addrCriteria.ai_protocol = IPPROTO_UDP;             // Only UDP protocol

            // Get address(es)
            struct addrinfo *servAddrs; // List of server addresses
            int rtnVal = getaddrinfo(server, servPort, &addrCriteria, &servAddrs);
            if (rtnVal != 0)
                DieWithUserMessage("getaddrinfo() failed", gai_strerror(rtnVal));  
            // Create a datagram/UDP socket
            int sock = socket(servAddrs->ai_family, servAddrs->ai_socktype,servAddrs->ai_protocol); // Socket descriptor for client
            if (sock < 0)
                DieWithSystemMessage("socket() failed");
            if(DEB)printf("\nFinita creazione socket udp\n");
            //FINE CREAZIONE SOCKET UDP
            
            //createMySocketUDP(&sock , dati_clnt.portaUDPserver, argv[1] , &addrCriteria, servAddrs);  da sistemare

            sleep(10);
            //creazione struttura per la ricezione del server
            
            if(DEB)printf("\nEntro nel while ed invio la prima stringa\n");
            int s = 1;
            sleep(4);

            
            ssize_t numBytes;
            size_t remlen  = (*frame).imageSize;
            void *curpos;
            int len;
            size_t sendlen = (*frame).imageSize;
            struct sockaddr_storage fromAddr; // Source address of server
            socklen_t fromAddrLen = sizeof(fromAddr);
            while(s){
                close(mypipefd[1]);
                read(mypipefd[0], buf, 15);   
                if(!strcmp(buf, "stop_figlio")){
                   if(DEB)printf("\nRicevuto stop_udp sul figlio e ora lo spengo..\n");
                       s = 0; //cambio s così termina la funzione ed esco dal ciclo
                }else{
                  
                    numBytes = sendto(sock, echoString, echoStringLen, 0, servAddrs->ai_addr, servAddrs->ai_addrlen);
                    //fromAddrLen = 
    
                    sendlen = (*frame).imageSize;
                    remlen  = (*frame).imageSize;
                    curpos = frame_buffer;

                    while (remlen > 0)
                    {
                        len = recvfrom(sock, curpos, 1024, 0 , (struct sockaddr *) &fromAddr, &fromAddrLen);
                        //if (len == -1)
                          //  return -1;

                        curpos += len;
                        remlen -= len;
                        sendlen = MIN(remlen, 1024);
                    }
                   
                    
                    (*frame).imageData = frame_buffer; 
                    
                        cvShowImage("Window",frame);
                        cvWaitKey(1);
                   // }

                    //sleep(2);
                    /*frame_buffer[0] = '\0';
                    ssize_t bytes_recv = read(sockTCPvideo, frame_buffer , 921600); //funzione nuova
                    printf("\nbytes_recv = %i\n", (int)bytes_recv); 

                    printf("\nPrima del recvfrom\n");
                    */
                    
                    //funzione del client poi dovrò modificarla
                    
                   /* if (sendto(sockfd, "ciao" , sizeof("ciao") , 0, (struct sockaddr *)&servaddr, addr_size )<0){
                        printf("sendto error\n"); exit(1);
                    } */
                    //len = sizeof(servaddr); //per la ricezione
                    //if(DEB)printf("\nStringa di prova inviata dentro al processo figlio \n");
                    //size_t kn = recvfrom(sockfd, frame_buffer, 921600, 0, NULL, NULL );
                    
                    //if(DEB)printf("\nLunghezza buffer ricevuto %i\n", (int)kn );

                    
                    //client_echo_udp(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));                    

                /*    (*frame1).imageData = frame_buffer; 
                    if(DEB)printf("\nDopo l assegnazione dell immagine\n");
                    //cvSetData((*frame1).imageData , frame_buffer, 921600);
                    //memcpy( (*frame1).imageData , frame_buffer, 921600);                 
                    int tr = 100;
                    while(tr){ 
                        tr--; 
                        cvShowImage("Window",frame1);
                        cvWaitKey(1);
                    } */
                }
            } //fine del while s
            
   
            //CHIUSURA DI TUTTE LE COSE APERTE DENTRO AL FIGLIO
            free(frame_buffer);
            cvDestroyAllWindows();
            close(sock);
            
            if(DEB)printf("\nProcesso figlio finito\n");
            exit(0);                     
    } else {
        /*********************************************** parent process ******************************************************/
        if(DEB)printf("\nParent process\n"); 
         
        comandiTCP( mypipefd, &dati_clnt);
        wait();
        if(DEB)printf("\nParent process terminato\n");
        exit(0);
    } //fine processo padre
    cvDestroyAllWindows();
    close(sockTCP);                             //chiusura socket TCP
    free(frame);                                //libera la memoria allocata per l header del frame 
    if(DEB)printf("\nPipe terminate\n");
    return 0;
}



