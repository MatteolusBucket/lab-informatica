/**
 * @file     funzioni_client.h
 * @brief    Libreria di un client 
 * 
 * @author   Matteo Maso (<matteo.maso.1@studenti.unipd.it>)
 * @version  1.0
 * @since    1.0
 *
 * @details  Libreria per un client che si collega ad un server via tcp, acquisisce dati a video con udp e manda comandi al server
 */

struct DATI_CLNT{

    int stato_video;
    int sock_TCP;                                     //sockTCP    
    int stato_udp;                                    //stato connessione udp accesa o spenta
    struct sockaddr_in   *sockUDPserver_pointer;      //punta alla struttura dati udp del server********da verificare ******
    socklen_t            len_sockUDP;                 //contiene i dati della lunghezza della struttura *****da verificare
    in_port_t portaUDPclnt;                           //ci salvo l argv 2 che contiene la porta udp del client
    char *portaUDPserver;                         //salvo la porta udp server
     

    int sinc_status;                //stato della sincronizzazione se 0 ok se negativo no buono
    //dati immagine server
    IplImage *frame;                //puntatore alla struttura IplImage
    int imageSize;
    int nChannel;
    int width;
    int height;
    int size_Header;        //dimensione header in bytes
    
};


/**
 * @brief return a message on stderr
 *
 * @param msg    the address of the first value. The value will not be modified.
 * @param detail the address of the second value. The value will not be modified.
 */
void DieWithUserMessage(const char *msg, const char *detail);

/**
 * @brief return a message 
 *
 * @param msg    the address of the value. The value will not be modified.
 */
void DieWithSystemMessage(const char *msg);

/**
 * @brief INVIO STRINGA TRAMITE TCP
 *
 * @param command  array che termina per '\0'               
 * @param sock     puntatore ad una sock                    gli passo direttamente il valore della socket da usare
 * @details        invio la stringa terminante con '\0'
 */  
void send_TCP(char *command, int sock); 

/** 
 * @brief RICEZIONE STRINGA TRAMITE TCP
 *
 * @param length     se ho int x = 0; gli passo &x
 * @param sock       puntatore ad una sock se ho sock = socket() gli passo sock
 * @param buffer     in teoria se alloco un buffer con void *buf = malloc(x); gli passo buf se ho char buffer[] passo &buffer
 * @param bufLength  bufLength è la lunghezza max del baffer in cui scrivo 
 * @details          ricevo su sock la stringa di lunghezza length e lunghezza massima buflength
 */ 
void recv_TCP(char *buffer, int sock, int *length, int bufLength );

/**
 * @brief   CREA UNA SOCKET client TCP 
 *
 * @param   servSock        punta ad un descrittore socket
 * @poram   port            passo argv[1] esempio  oppure se ho char porta[] = "4445" gli passo porta
 * @param   ip              passo argv[1] esempio  oppure se ho char ip[] = "192.168.1.2" gli passo porta
 *        
 * @return                  0 if ok -1 if errore
 */
int createMySocketTCP(int *servSock , char port[], char ip[]);

/**
 * @brief restituisce su *command una stringa da stdin terminante con '\0'
 *
 * @param command           the address of the firs variable
 * @details restituisce la stringa acquisita da stdin sostituendo il '\n' finale con un '\0' al interno dell'indirizzo puntato da command
 */ 
int keep_command(char *command);                           //acquisisce comando da stdin



//************************************** FINE SISTEMAZIONE ******************************************************************
/**
 * @brief RICEVO COMANDI ED ESEGUO FUNZIONI 
 *
 * @param pipefd        punta ad una pipe
 * @param dati_clnt     punta ad una struttura dati
 */
void comandiTCP(int *pipefd, struct DATI_CLNT *dati_clnt);

/**
 * @brief print_webcam imposta i parametri della fotocamera
 *
 * @param cv_cap  valore da cvCaptureFromCAM(0);
 */
//void print_webcam_info(CvCapture* cv_cap);


/**
 * @brief FUNZIONE DI SINCRONIZZAZIONE lato CLIENT
 * 
 * @param sock          socket TCP del server in cui ascoltare le risposte
 * @param structDati    gli passo il nome del puntatore alla strututra dati
 */
void connessioneServer(int sock, struct DATI_CLNT *dati_pointer);

/**
 * @brief recv che smette di ricevere solo dopo aver ricevuto n bytes
 */
ssize_t readn (int fd, char *buf, size_t n);

/**
 * @brief funzione che manda richieste al server e riceve risposte da modificare
 */
void client_echo_udp( int sockfd, const struct sockaddr *p_servaddr, socklen_t servlen);

//********************************************* NUOVA PARTE *************************************************************

/**
 * @brief   CREA UNA SOCKET client TCP 
 *
 * @param   sock            punta ad un descrittore socket
 * @poram   port            stringa contenente la porta a cui collegarsi 
 * @param   ip              stringa contenente l indirizzo del server
 *        
 * @return                  0 if ok -1 if errore
 */
int createTCP(int *sock , char port[], char ip[]);


/**
 *@brief Legge sulla pipe in modo bloccante
 *
 *@param fd     descrittore pipe
 *@param buf    stringa di lunghezza massima MAX_PIPE_LEN
 *
 */
void read_pipe_block(int fd[], char *buf);

/**
 *@brief Legge sulla pipe in modo non bloccante  
 *
 *@param fd     descrittore pipe
 *@param buf    stringa di lunghezza massima MAX_PIPE_LEN
 *
 *@return int   torna -1 se non ha letto 0 altrimenti
 */
int read_pipe_unblock(int fd[], char *buf);

/**
 *@brief Scrive sulla pipe
 *
 *@param fd         descrittore pipe
 *@param buf        stringa di lunghezza massima MAXPIPE_LEN
 *@param buf_len    lunghezza buffer da scrivere
 *
 */
void write_pipe(int fd[], char *buf, int buf_len);

/**
 * @brief FUNZIONE DI SINCRONIZZAZIONE lato CLIENT
 * 
 * @param sock          socket TCP del server in cui ascoltare le risposte
 * @param structDati    gli passo il nome del puntatore alla strututra dati
 *
 * @return int          se sincronizzazione ha avuto successo 0 altrimenti -1
 */
int connect_server_TCP(int sock,  struct DATI_CLNT *dati_pointer);

/**
 * @brief   CREA UNA SOCKET clnt UDP da sistemare l header ***************************************************************************
 *
 * @param   socketName      punta ad un descrittore socket
 * @poram   port            passo argv[1] esempio  oppure se ho char porta[] = "4445" gli passo porta
 *
 * @return                  0 if ok -1 if errore
 */
void createMySocketUDP(int *sock , in_port_t port, char *indirizzo, struct addrinfo *addrCriteria, struct addrinfo *servAddrs);
