/**
 * @file     funzioni_client.c
 * @brief    Libreria di un client 
 * 
 * @author   Matteo Maso (<matteo.maso.1@studenti.unipd.it>)
 * @version  1.0
 * @since    1.0
 *
 * @details  Libreria per un client che si collega ad un server via tcp, acquisisce dati a video con udp e manda comandi al server
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdbool.h>

//per la udp da ridefinire in base a quello che mi serve


/*librerie per opencv */
#include <cv.h>
#include <highgui.h>
#include <cxcore.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/videoio/videoio_c.h>
#include <opencv2/highgui/highgui_c.h>
/* declares the funcions provided by this library */
#include "funzioni_client.h"

#define MAXSTRINGLENGTH 70
#define DEB     1 //se DEB = 1 allora sono abilitati i print del debug
#define SCRITTE 1
#define MAX_PIPE_LEN 50 //lunghezza massima buffer pipe
#define stdin_len 200   //lunghezza massima da acquisire in input

/**
 * @brief return a message on stderr
 *
 * @param msg    the address of the first value. The value will not be modified.
 * @param detail the address of the second value. The value will not be modified.
 */
void DieWithUserMessage(const char *msg, const char *detail) {
    fputs(msg, stderr);
    fputs(": ", stderr);
    fputs(detail, stderr);
    fputc('\n', stderr);
    exit(1);
}

/**
 * @brief return a message 
 *
 * @param msg    the address of the value. The value will not be modified.
 */
void DieWithSystemMessage(const char *msg) {
    perror(msg);
    exit(1);
}

/**
 * @brief INVIO STRINGA TRAMITE TCP
 *
 * @param command  array che termina per '\0'               
 * @param sock     puntatore ad una sock                    gli passo direttamente il valore della socket da usare
 * @details        invio la stringa terminante con '\0'
 */  
void send_TCP(char *command, int sock){
             
    size_t  strLen   = strlen(command);                    //determina la lunghezza della stringa da mandare      
    ssize_t numBytes = send(sock, command, strLen, 0);     //invia la stringa al server 
    //if(DEB)printf("\nLunghezza della stringa inviata in bytes: %d", numBytes);
    if(numBytes < 0 )
        DieWithSystemMessage("send() failed");
    else if (numBytes != strLen)
        DieWithUserMessage("send()", "sent unexpected number of bytes");
} 

/** 
 * @brief RICEZIONE STRINGA TRAMITE TCP
 *
 * @param length     se ho int x = 0; gli passo &x
 * @param sock       puntatore ad una sock se ho sock = socket() gli passo sock
 * @param buffer     in teoria se alloco un buffer con void *buf = malloc(x); gli passo buf se ho char buffer[] passo &buffer
 * @param bufLength  bufLength è la lunghezza max del baffer in cui scrivo 
 * @details          ricevo su sock la stringa di lunghezza length e lunghezza massima buflength
 */ 
void recv_TCP(char *buffer, int sock, int *length, int bufLength ){

    //ssize_t recv(int socket, void *buffer, size_t length, int flags);               
    //dovrei verificare cosa succede se mando più cose di bufLength
    //in caso le faccio scartare e dico stringa troppo lunga

    ssize_t numBytes = recv(sock, buffer, (size_t)bufLength, 0 );   //inviata la stringa
    *length = numBytes;                                             //inserito il numero di bytes ricevuti all indirizzo di length

    if(numBytes < 0 )
        DieWithSystemMessage("send() failed");
    //devo aggiornare l ultimo carattere solo se diverso da zero se il numBytes -1 = '\0' ok altrimenti lo metto io 
    if(DEB)printf("\nnumBytesrecv = %i\n", (int)numBytes);            
    if(!((buffer[numBytes - 1]) == '\0') && numBytes > 0){ //se l ultimo carattere nn è '\0' ma ho ricevuto bytes
        if(numBytes < bufLength){   //sistemo l ultimo indice se non raggiungo la dimensione max         
            buffer[numBytes] = '\0';        //devo verificare se punta bene a quel che deve puntare 
        }else{
            DieWithUserMessage(" 0 buffer", "dimensione massima buffer raggiunta");    
        }
    }
}    

/**
 * @brief   CREA UNA SOCKET client TCP 
 *
 * @param   servSock        punta ad un descrittore socket
 * @poram   port            passo argv[1] esempio  oppure se ho char porta[] = "4445" gli passo porta
 * @param   ip              passo argv[1] esempio  oppure se ho char ip[] = "192.168.1.2" gli passo porta
 *        
 * @return                  0 if ok -1 if errore
 */
int createMySocketTCP(int *servSock , char port[], char ip[]){


    int servSock3;                                                   // Socket descriptor for server
    servSock3 = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (servSock3 < 0) 
        DieWithSystemMessage("socket() failed");        //errore nella creazione della socket
    

   // char *servIP = ip; //imposto l ip

    in_port_t servPort = atoi(port);
    
    // Construct the server address structure
    struct sockaddr_in servAddr;                // Server address
    memset(&servAddr, 0, sizeof(servAddr));     // Zero out structure
    servAddr.sin_family = AF_INET;              // IPv4 address family
    servAddr.sin_port = htons(servPort);        // Server port   

    int rtnVal = inet_pton(AF_INET, ip, &servAddr.sin_addr.s_addr);          //conversione dell'indirizzo e della porta
    if (rtnVal == 0)                                                         //se la conversione ha fallito torna errore
        DieWithUserMessage("inet_pton() failed", "invalid address string");
    else if (rtnVal < 0)
        DieWithSystemMessage("inet_pton() failed");                          //se conversione ha fallito errore
    
    if(connect(servSock3, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)   //establish the connection to the server
        DieWithSystemMessage("connect() failed");

    
    *servSock = servSock3; //ritorno la socket

    return 0;
} 

/**
 * @brief restituisce su *command una stringa da stdin terminante con '\0'
 *
 * @param command  the address of the firs variable
 * @details restituisce la stringa acquisita da stdin sostituendo il '\n' finale con un '\0' al interno dell'indirizzo puntato da command d massimo 20 caratter (o 19)
 */
int keep_command(char *command){


    fflush(stdin);                                      //pulisce lo stdin
    //acquisizione di stringa da stdin di una lunghezza massima prestabilitae e tolgo il \n finale
    char *stringa = (char *)malloc(sizeof(char) * stdin_len);   
    if (stringa == NULL) {                              //controllo se ho allocato bene la memoria
        printf("Allocation failed.\n");
    }
    fgets(stringa, stdin_len, stdin);                          //acquisisce al max 19 caratteri
    stringa[strlen(stringa) -1 ] = '\0';                //sistemo l ultimo carattere altrimenti sarebbe '\n'
    strcpy(command, stringa);
    //devo inserire un modo perchè vengano eliminate le stringhe oltre i 20 caratteri
    free(stringa);                                      //dealloca la memoria usata
    return 0;                                           //restituisce 0 se tutto ok 
    
}

//************************************************ FINE SISTEMAZIONE **************************************************
/**
 * @brief RICEVO COMANDI ED ESEGUO FUNZIONI 
 *
 * @param pipefd        punta ad una pipe
 * @param dati_clnt     punta ad una struttura dati
 */
void comandiTCP(int pipefd[], struct DATI_CLNT *dati_clnt){

    int ctrl = 1;
    while(ctrl){
    sleep(1); 
    char command[20];                //buffer del coman
    char *command_pointer = command; //puntatore al commando

    //stampare la richiesta di inserimento comando
    printf("\nINSERISCI COMANDO DESIDERATO oppure help: ");         
    int x1 = keep_command(command_pointer);     //acquisisci comando da stdin */
        
        // Inizio elaborazione comando */
    if(!strcmp(command, "help")){       //se command == help potrei metterlo in un file e farlo aprire
            //if(DEB)printf("\nStringa uguale a help\n");           
            printf("\nLista comandi possibili:\n");         
            printf("-exit         per uscire da questo programma\n");
            printf("-server_stop  per far spegnere il server (consigliato prima di uscire) \n");
            
        }else if(!strcmp(command, "exit")){  //se command == exit
            ctrl = 0;
            if(DEB)printf("\nStringa uguale a exit uscita dal programma in corso...\n");
            close(pipefd[0]);                     //mette la pipe in scrittura
            write(pipefd[1], "stop_figlio", 15);     //scrivo sulla pipe di chiudere il video
                        
            sleep(3); //importante non spegnere subito il server altrimenti il figlio resta bloccato perchè on riceve più nulla
            send_TCP("server_stop", (*dati_clnt).sock_TCP );    //se spengo il client si spegne anche il server         
            //devo anche spegnere tutte le eventuali cose che ho aperto e mandare lo spegnimento al server
            sleep(1);
        }else if(!strcmp(command, "server_stop")){
            //printf("\nServer_spento no ancora implementato\n");
            
            send_TCP("server_stop", (*dati_clnt).sock_TCP );    //verificare se funziona
        }else{  //funzione di default
            printf("\nNessuna stringa valida inviata, riprovare!\n");
            printf("\nStringa non valida -%s-\n", command);
        }
    } //uscito dal while */

}





/**
 * @brief   CREA UNA SOCKET clnt UDP  da sistemare
 *
 * @param   socketName      punta ad un descrittore socket
 * @poram   port            passo argv[1] esempio  oppure se ho char porta[] = "4445" gli passo porta
 *
 * @return                  0 if ok -1 if errore
 */
void createMySocketUDP(int *sock , in_port_t port, char *indirizzo, struct addrinfo *addrCriteria, struct addrinfo *servAddrs){

    (*addrCriteria).ai_family = AF_UNSPEC;  
    (*addrCriteria).ai_socktype = SOCK_DGRAM;              // Only datagram sockets
    (*addrCriteria).ai_protocol = IPPROTO_UDP;             // Only UDP protocol
    char stra[8];
    snprintf(stra, 8,"%i" , (int)port);    
    int rtnVal = getaddrinfo(indirizzo, stra , addrCriteria, &servAddrs);
    if (rtnVal != 0)
           DieWithUserMessage("getaddrinfo() failed", gai_strerror(rtnVal));
    
    int socknew = socket(servAddrs->ai_family, servAddrs->ai_socktype,servAddrs->ai_protocol); // Socket descriptor for client
    if (socknew < 0)
           DieWithSystemMessage("socket() failed");
    *sock = socknew; 
}



/**
 * @brief FUNZIONE DI SINCRONIZZAZIONE lato CLIENT
 * 
 * @param sock          socket TCP del server in cui ascoltare le risposte
 * @param structDati    gli passo il nome del puntatore alla strututra dati
 */
void connessioneServer(int sock,  struct DATI_CLNT *dati_pointer){

    if(DEB)printf("\nfunzione connessione con Server in corso... \n");
    
    int bufsize_stdin = 200;     //forse meglio dichiararlo fuori poi vediamo
    int bufTCP_size   = 2000;    //dimensione massima buffer di ricezione da tcp
    char *buffer      = (char *)malloc(bufsize_stdin);       //allocazione dimanica buffer std_in 
    char *bufTCP      = (char *)malloc(bufTCP_size);         //allocazione buffer ricezione comando
    
    char lista_comandi[] = "comandi implementati lato client: \
\n -help_server                mi restituisce la lista di comandi implementati solo lato server\
\n -help                       restituisce tutti i comandi possibili\
\n -sinc                       si sincronizza col server\
\n -start                      fa partire il processo di acquisizione video\
\n -server_stop                cessa la sincronizzazione del server\
\n -client_stop                cessa la sincronizzazione senza aver finito\
\n ";

    char *separate = " ";
    
    int setup = 0;
    while(!setup){
        //qui devo ricevere dei comandi e alla ricezione eseguire delle azioni
        //esco dal while solo quando ricevo un determinato programma

        setup = 1;                    //significa setup eseguito oppure uscita 
        keep_command(buffer);         //acquisisco comando da stdin e lo metto in buffer
               
        int nBytesRecv = 0;           //numero di bytes ricevuti dalla socket TCP se serve usarli 
        char *arg1 = strtok(buffer, separate);
        char *arg2 = strtok(NULL, separate);
        //printf("\narg1 = -%1$s-\narg2 = -%2$s-\n", arg1, arg2); 
        /* Eseguo funzioni in base alla stringa ricevuta */      
        if(!strcmp( arg1, "client_stop")){
            if(SCRITTE)printf("\nRicevuta la stringa client_stop\n"); //lascio a zero control cosi esco dal ciclo
            send_TCP( "server_stop" , sock);                          //invio anche al server di smettere
        }else{
            setup = 0;   //imposto il control = 0 cosi proseguo ed elaboro le richieste              
            if(!strcmp( arg1, "help_server")){
                if(SCRITTE){ printf("\nStringa ricevuta = %s\n", buffer); }
                send_TCP( "help_server" , sock);
                recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size);               
                printf("\n%s\n", bufTCP);                    
                }else if(!strcmp( arg1, "help")){   //stampa i comandi possibili lato client e lato server
                    if(SCRITTE){ printf("\nStringa ricevuta = %s\n", buffer); }
                    send_TCP( "help_server" , sock);
                    recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size);             
                    printf("\n%s\nComandi lato client: \n%s", bufTCP, lista_comandi);                   
                }else if(!strcmp( arg1, "sinc")){   //inizia la sincronizzazione col server
                    if(DEB){ printf("\nStringa ricevuta = %s\n", arg1); }
                    if(SCRITTE){ printf("\nProcesso di sincronizzazione avviato...\n"); }
                    int ok = 1;                    
                    send_TCP( "sinc" , sock);       //server risponde ok_sinc
                    bufTCP[0] = '\0';     
                    recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size);
                    if(!strcmp( bufTCP, "ok_sinc")){ 
                        if(DEB)printf("\nok_sinc ricevuto");
                    }else{
                        printf("\nErrore nella sincronizzazione\n");
                        ok = 0;
                    }
                    if(ok){             //sincronizzo porta UDP client
                        char stra[24];
                        snprintf(stra, 24,"client_UDP_video %i" , (*dati_pointer).portaUDPclnt);                        
                        send_TCP( stra , sock);
                        bufTCP[0] = '\0';                       
                        recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size);                       
                        in_port_t pt = atoi(bufTCP); //variabile per confrontare la porta ricevuta
                        if(  pt == ((*dati_pointer).portaUDPclnt)){ 
                            if(DEB)printf("\nok porta corretta");
                        }else{
                            printf("\nErrore nella sincronizzazione della porta\n");
                            ok = 0;
                            send_TCP( "sincronizzazione_fallita" , sock);
                        }
                    }                   
                    if(ok){             //sincronizzo frame 
                        send_TCP( "get_Size_Header" , sock);
                        bufTCP[0] = '\0'; 
                        recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size); //riceve un intero contenente la dimensione dell array
                        if(DEB)printf("\nrecv size_header con successo\n");
                        (*dati_pointer).size_Header = atoi(bufTCP);       //scrivo la dim del header nella struttura dati
                        if(DEB)printf("\nDimensione header ricevuta= -%s-\n", bufTCP);                        
                        //se la dimensione ci sta nel buffer bufTCP = ok altrimenti ne alloco uno più grosso
                        send_TCP( "get_Header" , sock);
                        bufTCP[0] = '\0';
                        sleep(3);
                        recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size); //riceve un intero contenente la dimensione dell array
                        if(((*dati_pointer).size_Header) == nBytesRecv){ 
                            if(DEB)printf("\nsalvo header\n");
                            //copio l header ricevuto nel header che ho creato 
                            memcpy((*dati_pointer).frame, bufTCP, (*dati_pointer).size_Header); 
                        }else{
                            printf("\nErrore nella sincronizzazione del header\n");
                            if(DEB)printf("\nDimensione header prevista = %i\n", (*dati_pointer).size_Header);
                            if(DEB)printf("\nBytes ricevuti dalla connessione = %i\n", nBytesRecv);
                            ok = 0;
                            send_TCP( "sincronizzazione_fallita" , sock);
                        }                          
                    }
                    if(ok){             //dico al server che abbiamo finito di soncronizzarci
                        if(DEB)printf("\nSend fine sincronizzazione");
                        send_TCP( "sincronizzazione_finita" , sock);    
                        recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size);
                        if(!strcmp( bufTCP, "ok_fine_sincronizzazione")){ 
                            if(DEB)printf("\nok fine sincronizzazione\n");
                            (*dati_pointer).sinc_status = 1; //setto a 1 => sincronizzazione avvenuta con successo
                        }else{
                            printf("\nErrore nella fine della sincronizzazione\n");
                            ok = 0;
                            send_TCP( "sincronizzazione_fallita" , sock);
                        }
                    }                   
                }else if(!strcmp( arg1, "udp_port")){           //questa poi andrò ad eliminarla 
                    if(SCRITTE){ printf("\nStringa ricevuta = %s\n", buffer); }
                    if(DEB)printf("\nbuffer:  -%s-\n", arg2); 
                    char stringa[30];  //contengo la stringa da mandare al server 
                    snprintf(stringa, (sizeof(arg1) + sizeof(arg2) + 10) , "  %1$s %2$s  ", "client_UDP_port", arg2);   
                    send_TCP( stringa , sock);    //mando comando 
                    recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size);
                    if(!strcmp(bufTCP , "porta acquisita con successo")){
                        if(DEB)printf("\nPorta acquisita bene\n");
                    }else{
                        printf("\nPorta acquisita male\n");
                        //eventualmente effettuare un controllo errori e fare qualcosa                    
                    }                    
                    
                            
                    //inserire un controllo stringa presa bene              
                  //  printf("\nPorta Udp server: %s\n", bufTCP);                  
                }else if(!strcmp( arg1, "start")){
                    printf("\nAvvio sincronizzazione...\n");
                                        
                    //controllo se (*dati_pointer).sinc_status = 1; è a 1 ok altrimenti smetto 
                    //problema nella sincronizzazione

                    send_TCP( "porta_udp_server" , sock);               //ottengo la porta udp del server
                    recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size);                      
                    (*dati_pointer).portaUDPserver = bufTCP;      
                    if(DEB)printf("\nPorta server UDP = -%s-\n", (*dati_pointer).portaUDPserver);
                    
                    char stringa[40];                                   //faccio conoscere al server la mia porta udp
                    snprintf(stringa, (sizeof(arg1)+20) , "  %1$s %2$i  ", "client_UDP_port", (*dati_pointer).portaUDPclnt);   
                    send_TCP( stringa , sock);    //mando comando 
                    recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size);
                    if(!strcmp(bufTCP , "porta acquisita con successo")){
                        if(DEB)printf("\nPorta acquisita bene\n");
                    }else{
                        printf("\nPorta acquisita male\n");
                        //eventualmente effettuare un controllo errori e fare qualcosa                    
                    }


                    long int wait = 999999999;
                    while(wait){ wait--; }
                    printf("\nStringa avvio_udp mandata\n");
                    send_TCP( "avvio_udp" , sock);                      //quando mando questa stringa il server esce dal ciclo 
                    recv_TCP(bufTCP, sock, &nBytesRecv, bufTCP_size); 
                    if(!strcmp(bufTCP , "fine_sinc_avvenuta_con_successo")){
                        if(DEB)printf("\nProcedura sincronizzazione avvenuta con successo\n");
                        setup = 1;                          //cosi posso uscire dal while
                        (*dati_pointer).stato_video = 1;    //variabile che mi indica che posso partire con udp 
                    }else{
                        printf("\nProcedura di sincronizzazione non andat a buon fine\n");
                        //eventualmente effettuare un controllo errori e fare qualcosa                    
                    }            
                    printf("\nInizio procedura video... \n");  


                
                }else if(!strcmp( arg1, "server_stop")){
                    if(SCRITTE){ printf("\nStringa ricevuta = %s\n", buffer); }  
                    send_TCP( "server_stop" , sock); 
                    
                }else{
                    if(SCRITTE) printf("\nStringa ricevuta = %s non valida\n", buffer);
                   // send_TCP( "stringa_non_valida" , sockClient);
                }
            } //fine else analisi stringhe ricevute

    } //funzione di setup completata
    free(bufTCP); //libero la memoria allocata per il buffer TCP
    free(buffer); //dealloco la memoria del buffer per stdin

    //devo anche settare un parametro per avviare o meno i due processi finito questa funzione se esco senza averli settati devo settarli di default
}


ssize_t readn (int fd, char *buf, size_t n){
    size_t nleft; ssize_t nread;
    nleft = n;
    while (nleft > 0) {
        if ( (nread = read(fd, buf+n-nleft, nleft)) < 0) {
            if (0)
                return(-1); // restituisco errore
        }
        else if (nread == 0) {
            // EOF, connessione chiusa, termino
            // esce e restituisco il numero di byte letti
            break;
        }
        else // continuo a leggere
            nleft -= nread;
    }
    return(n - nleft); // return >= 0
}

void client_echo_udp( int sockfd, const struct sockaddr *p_servaddr, socklen_t servlen){
        
    int MAXLINE = 5;

    int n;
    char recvline[MAXLINE + 1], buff[MAXLINE];

    char *sendline = "ciao";
    
    socklen_t len;
    struct sockaddr *p_replyaddr;
    struct sockaddr_in *sa;
    p_replyaddr = malloc(servlen);
    while (1) {
        if (sendto(sockfd, "ciao" , sizeof("ciao") , 0, p_servaddr, servlen)<0){
            printf("sendto error\n"); exit(1);
        }
        len = servlen;
        if( (n = recvfrom(sockfd, recvline, MAXLINE, 0, p_replyaddr, &len)) < 0 ){
            printf("recvfrom error\n"); exit(1);
        }
        if( (len != servlen) || memcmp(p_servaddr, p_replyaddr, len) != 0 ) {
            sa = (struct sockaddr_in *) p_replyaddr;
            printf("risposta da %s ignorata\n", inet_ntop(AF_INET, &sa->sin_addr, buff, sizeof(buff)));
            continue;
        }
        recvline[n] = '\0';
        //fputs(recvline, stdout);
        printf("\n Ricevuta stringa sul figlio: %s\n", recvline);
    }
}

//********************************************* NUOVA PARTE *************************************************************
/**
 * @brief   CREA UNA SOCKET client TCP 
 *
 * @param   sock            punta ad un descrittore socket
 * @poram   port            stringa contenente la porta a cui collegarsi 
 * @param   ip              stringa contenente l indirizzo del server
 *        
 * @return                  0 if ok -1 if errore
 */
int createTCP(int *sock , char port[], char ip[]){
    
    int socket_descriptor;                                                  // Socket descriptor for server
    socket_descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);          //creazione socket TCP
    if (socket_descriptor < 0) 
        DieWithSystemMessage("socket() failed");                            //errore nella creazione della socket
    
    // Construct the server address structure
    struct sockaddr_in servAddr;                // Server address
    memset(&servAddr, 0, sizeof(servAddr));     // Zero out structure
    servAddr.sin_family = AF_INET;              // IPv4 address family
    servAddr.sin_port   = htons(atoi(port));    // Server port   

    int rtnVal = inet_pton(AF_INET, ip, &servAddr.sin_addr.s_addr);          //creazione del indirizzo da stringhe 
    if (rtnVal == 0)                                                         //se la conversione ha fallito torna errore
        DieWithUserMessage("inet_pton() failed", "invalid address string");
    else if (rtnVal < 0)
        DieWithSystemMessage("inet_pton() failed");                          //se conversione ha fallito errore
    
    if(connect(socket_descriptor, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)  //establish the connection to the server
        DieWithSystemMessage("connect() failed");   //se fallisce manda un messaggio d errore al sistema e termina il programma 

    *sock = socket_descriptor;                                      //ritorno la socket descriptor al programma chiamante

    //forse dovrei liberare la memoria allocata con memset ma nn so se poi funziona il resto 
    return 0;
}

/**
 *@brief Scrive sulla pipe
 *
 *@param fd         descrittore pipe
 *@param buf        stringa di lunghezza massima MAXPIPE_LEN
 *@param buf_len    lunghezza buffer da scrivere
 *
 */
void write_pipe(int fd[], char *buf, int buf_len){


    printf("\nBuf dentro a write pipe -%s-\n", buf);
    if(!(sizeof(buf) < MAX_PIPE_LEN) ){
        //printf("\n Stringa troppo lunga da scrivere\n");
        exit(0);
    } else {
        //close(fd[0]);   //chiude il lato in lettura
        //printf("\nbuf dentro a write_pipe -%1$s- dimensione %2$i\n", buf, buf_len );
        
        int s = 1;
        int control;
        while(s){
            close(fd[0]);   //chiude il lato in lettura
            control = write(fd[1], buf, buf_len);
            if(control < 0){
                sleep(1); //aspetta n attimo e poi riprova
                printf("\nProblema scrittura pipe\n");
            }else{
                s = 0;
            }
        }
        
    }
}


/**
 *@brief Legge sulla pipe in modo bloccante fin che nn legge qualcosa
 *
 *@param fd     descrittore pipe
 *@param buf    stringa di lunghezza massima MAX_PIPE_LEN
 *
 */
void read_pipe_block(int fd[], char *buf){

    int control;
    int s = 1;    
    while(s){
        sleep(1);
        buf[0] = '\0';  //azzero il buffer
        close(fd[1]);
        control = read(fd[0], buf, MAX_PIPE_LEN);
        //printf("\nBuf dentro a read_pipe %s\n", buf);
               
        if(control < 0){
            if(DEB)printf("\nErrore nella lettura delle pipe r_dad\n");
        } else {
            s = 0;
        }
        
        
    }
}

/**
 *@brief Legge sulla pipe in modo non bloccante  
 *
 *@param fd     descrittore pipe
 *@param buf    stringa di lunghezza massima MAX_PIPE_LEN
 *
 *@return int   torna -1 se non ha letto 0 altrimenti
 */
int read_pipe_unblock(int fd[], char *buf){

    int control;
    int s = 1;    
    
        
    buf[0] = '\0';  //azzero il buffer
    close(fd[1]);
    control = read(fd[0], buf, MAX_PIPE_LEN);
    //printf("\nBuf dentro a read_pipe %s\n", buf);
               
    if(control < 0){
        //if(DEB)printf("\nErrore nella lettura delle pipe r_dad\n");
        return(-1);
    } else {
        return(0);
    }   
}

/**
 * @brief FUNZIONE DI SINCRONIZZAZIONE lato CLIENT
 * 
 * @param sock          socket TCP del server in cui ascoltare le risposte
 * @param structDati    gli passo il nome del puntatore alla strututra dati
 *
 * @return int          se sincronizzazione ha avuto successo 0 altrimenti -1
 */
int connect_server_TCP(int sock,  struct DATI_CLNT *dati_pointer){

    if(DEB)printf("\nFunzione connessione con Server in corso... \n");
    /* allocazione memoria e definizione variabili */ 
    int buf_len = 2000;                                         //lunghezza bufTCP 
    char *buf = (char *)malloc(sizeof(char) * buf_len);         //alloco il buffer di ricezione
    int BytesRecv = 0;                                          //numero di bytes ricevuti dalla socket TCP
    char *separate = " ";                                       //imposto i separatori con cui acquisire gli input
    char *input = (char *)malloc(sizeof(char) * stdin_len);     //alloco memoria per un buffer di input

    int x = -1;  //parametro restituito, mettere -1 per fall mentr 0 per ok

    /* Mi connetto col server altrimenti esco */
    send_TCP( "ack0" , sock);                     //mando un ack0
    recv_TCP(buf, sock, &BytesRecv, buf_len);     //aspetto un ack1
    if(!strcmp( buf, "ack1")){
        //ok posso partire col programma 
        x = 0;
    }else {
        if(DEB)printf("\nProblema nella sincronizzazione chiusura in corso...\n"); 
        x = -1;
    }

    char lista_comandi[] = "comandi implementati lato client: \
\n\t -webcam                     mi restituisce la lista delle webcam disponibili\
\n\t -help                       restituisce tutti i comandi possibili\
\n\t -getcam parametro           si sincronizza per avere quel video\
\n\t -server_stop                cessa la sincronizzazione del server\
\n\t -exit                       cessa la sincronizzazione senza aver finito\
\n\t ";

    int setup;          //paramentro di controllo ciclo while
    if(x == 0){
        setup = 0; //cosi entro nel while ed effettuo le cose altrimenti no
    } else {
        setup = 1; //così non entro nel while e cessa la funzione
    }
    while(!setup){
        
        buf[0] = '\0';             //resetto il buffer
        keep_command(input);          //acquisisco comando da stdin e lo metto in buffer
        BytesRecv = 0;                //numero di bytes ricevuti dalla socket TCP se serve usarli

        char *arg1 = strtok(input, separate);   //acquisisco la prima stringa del input
        char *arg2 = strtok(NULL, separate);    //acquisisco la seconda stringa dell input
        //printf("\narg1 = -%1$s-\narg2 = -%2$s-\n", arg1, arg2); 
        
        /* Eseguo funzioni in base alla stringa ricevuta */      
        if(!strcmp( arg1, "exit")){
            if(SCRITTE)printf("\nComando acquisito: %s\n", arg1); 
            send_TCP( "client_stop" , sock);  //dico al server che mi son spento
            setup = 1; //cosi esco dal while e termino
            x = -1;    //ritorna un insuccesso xkè non ho chiuso in modo appropriato 
        }else if (!strcmp( arg1, "webcam")){
            if(SCRITTE)printf("\nChiedo le webcam disponibili al server\n");
            send_TCP("lista_webcam", sock);                 //chiedo al server le webcam disponibili 
            recv_TCP(buf, sock, &BytesRecv, buf_len);       //aspetto la lista delle webcam disponibili 
            printf("\nLista webcam ricevuta: \n%s\n", buf); //stampo la lista delle webcam ricevuta
        }else if (!strcmp( arg1, "help")){
            printf("\nComandi lato client: \n%s", lista_comandi); 
        }else if (!strcmp( arg1, "getcam")){
            
            send_TCP( "porta_udp_server" , sock);               //ottengo la porta udp del server
            recv_TCP(buf, sock, &BytesRecv, buf_len);
            memcpy((*dati_pointer).portaUDPserver, buf , sizeof(buf)  );
            char stra[25];
            snprintf(stra, 25,"get_webcam %s " , arg1);     //creo la stringa da mandare
            send_TCP(stra , sock);                          //invio get_webcam 0   il zero + variabile
            //devo rivere e immagazzinare l header e dirgli che ho finito
            send_TCP( "get_Size_Header" , sock);            //chiedo la dimensione del header
            recv_TCP(buf, sock, &BytesRecv, buf_len);       //aspetto dimensione header
            (*dati_pointer).size_Header = atoi(buf);        //scrivo la dim del header nella struttura dati verifico se ci sta
            send_TCP( "get_Header" , sock);                 //chiedo l header
            recv_TCP(buf, sock, &BytesRecv, buf_len);       //aspetto header            
            if(((*dati_pointer).size_Header) == BytesRecv){ 
                if(DEB)printf("\nsalvo header\n");
                //copio l header ricevuto nel header che ho creato 
                memcpy((*dati_pointer).frame, buf, (*dati_pointer).size_Header);
                send_TCP( "sincronizzazione_ok" , sock); 
                }else{
                    printf("\nErrore nella sincronizzazione del header\n");
                    if(DEB)printf("\nDimensione header prevista = %i\n", (*dati_pointer).size_Header);
                    if(DEB)printf("\nBytes ricevuti dalla connessione = %i\n", BytesRecv);
                    send_TCP( "sincronizzazione_fallita" , sock);
                }        
            (*dati_pointer).sinc_status = 0; //ok sincronizzazione 
            setup = 1;      //così esco dal while con successo 
        }else if (!strcmp( arg1, "server_stop")){           //dovrei eseguirlo solo in modalità sudo ma nn introdotta
            //verifico se son sudo 
            send_TCP( "server_stop" , sock);                //invito il server a spegnersi
            recv_TCP(buf, sock, &BytesRecv, buf_len);       //aspetto spegnimento_server oppure non ho privilegi adeguati 
            //verificare ciò che ricevo 
            //poi se si spegne dovrei scrivere exit e spegnere anche il client
            if(DEB)printf("\nStringa ricevuta dal server in spegnimento: %s\n", buf);
        }else{ //eseguire di default
            printf("\nComando inesistente, prova con -help-\n");
        }
    } //funzione di setup completata

    free(input);  //dealloco memoria buffer input
    free(buf);    //dealloco la memoria per il buffer in ricezione
    return(x);    //-1 se fallimento mentre 0 se tutto ok 
}
